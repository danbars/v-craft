# @faasaf/v-craft

An attempt to provide an easily configurable page editor for vue3 components.

Originally this project is based on https://craft.js.org/ and its Vue2 port https://github.com/yoychen/v-craft.

The code of `@faasaf/v-craft` is based on https://github.com/loming/v-craft/tree/vue3, however it was heavily modified.

## Install

```bash
npm init vue@latest
cd vue-project/
echo @faasaf:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npx install-peerdeps @faasaf/v-craft
```
