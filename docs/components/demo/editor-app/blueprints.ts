import { Blueprints, CraftNode } from "@faasaf/v-craft";

export const blueprints: Blueprints = {
  CraftComponentSimpleContainer: {
    label: "Simple Container",
    componentName: "CraftCanvas",
    props: { component: "CraftComponentSimpleContainer" },
    children: [] as CraftNode[],
  },
  CraftComponentSimpleText: {
    label: "Text",
    componentName: "CraftComponentSimpleText",
    props: {
      content: "Change me.",
      tagName: "p",
    },
    children: [] as CraftNode[],
  },
  ContainerWithTextBlueprint: {
    label: "Container with Text",
    componentName: "CraftCanvas",
    props: {
      component: "CraftComponentSimpleContainer",
    },
    children: [
      {
        componentName: "CraftComponentSimpleText",
        props: {
          content: "Some prefilled text.",
          tagName: "h1",
        },
      },
    ],
  },
};
