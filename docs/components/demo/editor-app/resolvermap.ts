export const resolverMap = {
  paragraph: {
    component: "CraftComponentSimpleText",
    propsSchema: [
      {
        $formkit: "select",
        label: "Type",
        name: "tagName",
        options: [
          "h1",
          "h2",
          "h3",
          "h4",
          "h5",
          "h6",
          "p",
          "span",
          "div",
          "blockquote",
        ],
      },
      {
        $formkit: "textarea",
        label: "Content",
        name: "content",
      },
    ],
  },
  container: {
    component: "CraftComponentSimpleContainer",
  },
  canvas: {
    component: "CraftCanvas",
  },
};
