<script setup>
import Viewer from "./components/demo/editor-app/viewer.vue"
</script>

## Viewer Usage

<DemoContainer>
  <Viewer />
</DemoContainer>

<<< @/components/demo/editor-app/viewer.vue

<<< @/components/demo/editor-app/resolvermap.ts
