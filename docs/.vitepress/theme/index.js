import VCraft from "@faasaf/v-craft";
import { defaultConfig, plugin } from "@formkit/vue";
import { createPinia } from "pinia";
import DefaultTheme from "vitepress/theme";
import DemoContainer from "../components/DemoContainer.vue";

import "./custom.css";
import "../../../node_modules/@formkit/themes/dist/genesis/theme.css";

const pinia = createPinia();

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(pinia);
    app.use(VCraft);
    app.use(plugin, defaultConfig);
    app.component("DemoContainer", DemoContainer);
  },
};
