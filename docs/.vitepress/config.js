const path = require("path");

module.exports = {
  title: "faasaf/v-craft",
  base: process.env.PUBLIC_BASE || "/",
  description: "An attempt to deliver a Vue.js 3 page editor library.",
  themeConfig: {
    repo: "https://gitlab.com/faasaf/v-craft",
    sidebar: [
      { text: "Installation", link: "index" },
      {
        text: "Usage",
        children: [
          { text: "Editor", link: "/editor" },
          { text: "Viewer", link: "/viewer" },
        ],
      },
    ],
  },
  vite: {
    resolve: {
      alias: {
        "@faasaf/v-craft": path.resolve(__dirname, "../../src"),
      },
      dedupe: ["vue"],
    },
  },
};
