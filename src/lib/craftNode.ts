import { v4 as uuidv4 } from "uuid";

export type CraftNodeRules = {
  canMoveIn: (craftNode: CraftNode, targetNode: CraftNode) => boolean;
  canMoveOut: (craftNode: CraftNode, parentNode: CraftNode) => boolean;
  canDrag: (craftNode: CraftNode) => boolean;
};

export interface CraftNode {
  componentName: string;
  props: any;
  children: CraftNode[];
  parent: CraftNode | null;
  uuid: uuidv4;
  rules?: CraftNodeRules;
}

export const setCraftNodeProps = (craftNode: CraftNode, props: any) => {
  craftNode.props = { ...craftNode.props, ...props };
};

export const emancipateCraftNode = (craftNode: CraftNode) => {
  const { parent } = craftNode;

  if (!parent) {
    return;
  }

  const index = parent.children.indexOf(craftNode);
  parent.children.splice(index, 1);
  craftNode.parent = null;
};

export const setParent = (craftNode, parent) => {
  if (!craftNodeCanBeChildOf(craftNode, parent)) {
    throw new Error("Parent node is not droppable.");
  }

  emancipateCraftNode(craftNode);

  parent.children.push(craftNode);
  craftNode.parent = parent;
};

export const craftNodeInCanvas = (craftNode) => {
  let curentParent = craftNode.parent;

  while (curentParent) {
    if (craftNodeIsCanvas(curentParent)) {
      return true;
    }

    curentParent = curentParent.parent;
  }

  return false;
};

export const craftNodeIsDraggable = (craftNode) => {
  if (!craftNodeInCanvas(craftNode)) {
    return false;
  }

  if (craftNode.rules?.canDrag) {
    return craftNode.rules.canDrag(craftNode);
  }

  return true;
};

export const craftNodeIsAncestorOf = (craftNode, descendant) => {
  let curentParent = descendant.parent;

  while (curentParent) {
    if (curentParent === craftNode) {
      return true;
    }
    curentParent = curentParent.parent;
  }

  return false;
};

export const craftNodeCanBeChildOf = (craftNode, parent) => {
  if (!craftNodeIsCanvas(parent)) {
    return false;
  }

  if (craftNode === parent) {
    return false;
  }

  if (
    craftNode.parent &&
    craftNode.parent.rules?.canMoveOut &&
    !craftNode.parent.rules?.canMoveOut(craftNode, craftNode.parent)
  ) {
    return false;
  }

  if (craftNodeIsAncestorOf(craftNode, parent)) {
    return false;
  }

  if (parent.rules?.canMoveIn) {
    return parent.rules.canMoveIn(craftNode, parent);
  }

  return true;
};

export const craftNodeIsCanvas = (craftNode) => {
  return craftNode.componentName === "CraftCanvas";
};

export const appendCraftNodeTo = (
  craftNode: CraftNode,
  targetNode: CraftNode
) => {
  if (!craftNodeCanBeChildOf(craftNode, targetNode)) {
    throw new Error(
      `${craftNode.componentName} is not allowed to be a child of ${targetNode.componentName}.`
    );
  }

  emancipateCraftNode(craftNode);

  craftNode.parent = targetNode;
  targetNode.children.push(craftNode);

  return targetNode;
};

export const prependCraftNodeTo = (
  craftNode: CraftNode,
  targetNode: CraftNode
) => {
  if (!craftNodeCanBeChildOf(craftNode, targetNode)) {
    throw new Error(
      `${craftNode.componentName} is not allowed to be a child of ${targetNode.componentName}.`
    );
  }

  emancipateCraftNode(craftNode);

  targetNode.children.splice(0, 0, craftNode);
  craftNode.parent = targetNode;

  return targetNode;
};

export const craftNodeCanBeSiblingOf = (
  craftNode: CraftNode,
  targetNode: CraftNode
) => {
  if (targetNode === craftNode) {
    return false;
  }

  if (!targetNode.parent) {
    return false;
  }

  return craftNodeCanBeChildOf(craftNode, targetNode.parent);
};

export const insertCraftNodeBefore = (craftNode, targetNode) => {
  if (!craftNodeCanBeSiblingOf(craftNode, targetNode)) {
    throw new Error("Can not be the sibling of the target node.");
  }

  emancipateCraftNode(craftNode);

  const parentOfTargetNode = targetNode.parent;
  const indexOfTargetNode = parentOfTargetNode.children.indexOf(targetNode);
  parentOfTargetNode.children.splice(indexOfTargetNode, 0, craftNode);
  craftNode.parent = parentOfTargetNode;

  return parentOfTargetNode;
};

export const insertCraftNodeAfter = (craftNode, targetNode) => {
  if (!craftNodeCanBeSiblingOf(craftNode, targetNode)) {
    throw new Error("Can not be the sibling of the target node.");
  }

  emancipateCraftNode(craftNode);

  const parentOfTargetNode = targetNode.parent;
  const indexOfTargetNode = parentOfTargetNode.children.indexOf(targetNode);
  parentOfTargetNode.children.splice(indexOfTargetNode + 1, 0, craftNode);
  craftNode.parent = parentOfTargetNode;

  return parentOfTargetNode;
};

export const craftNodeEnsureTree = (craftNode: CraftNode): CraftNode => {
  if (!craftNode.children) {
    return craftNode;
  }

  craftNode.children.map((cn) => {
    if (!cn.uuid) {
      cn.uuid = uuidv4();
    }
    if (cn.parent !== craftNode) {
      cn.parent = craftNode;
    }

    if (cn.children) {
      cn.children.map((cnc) => craftNodeEnsureTree(cnc));
    }

    return cn;
  });

  return craftNode;
};
