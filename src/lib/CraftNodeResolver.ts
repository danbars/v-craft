import { FormKitSchemaNode } from "@formkit/core";
import kebabCase from "lodash/kebabCase";
import { CraftNode, craftNodeIsCanvas } from "./craftNode";

export interface CraftNodeComponentMap {
  component: string;
  propsSchema?: FormKitSchemaNode[];
  defaultProps?: Record<string, any>;
}

export type CraftNodeResolverMap = Record<string, CraftNodeComponentMap>;

class CraftNodeResolver {
  resolverMap: CraftNodeResolverMap = {};

  constructor(resolverMap: Record<string, CraftNodeComponentMap> = {}) {
    this.setResolverMap(resolverMap);
  }

  setResolverMap(resolverMap: Record<string, CraftNodeComponentMap>) {
    this.resolverMap = {};
    Object.entries(resolverMap).forEach(([key, value]) => {
      this.resolverMap[kebabCase(value.component)] = value;
    });
  }

  resolve(name: string): CraftNodeComponentMap {
    return this.resolverMap[kebabCase(name)];
  }

  getDefaultProps(craftNode: CraftNode): Record<string, any> {
    return this.resolveNode(craftNode)?.defaultProps || {};
  }

  resolveNode(craftNode: CraftNode): CraftNodeComponentMap {
    if (craftNodeIsCanvas(craftNode)) {
      return this.resolve(craftNode.props.component);
    }

    return this.resolve(craftNode.componentName);
  }

  getSchema(craftNode: CraftNode): FormKitSchemaNode[] {
    return this.resolveNode(craftNode)?.propsSchema || [];
  }
}

export default CraftNodeResolver;
