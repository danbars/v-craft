import { App } from "vue";
import * as components from "./components";

function install(app: App) {
  for (const key in components) {
    app.component(key, components[key]);
  }
}

export default { install };

export * from "./components";
export * from "./lib/model";

export { useEditor } from "./store/editor";
export type { EditorState, EditorStoreType } from "./store/editor";

export { useIndicator } from "./store/indicator";
export type { IndicatorState, IndicatorStoreType } from "./store/indicator";
